# python3 -m unittest discover tests
import unittest, random
import network_representation


class TestNetRep(unittest.TestCase):

    def test_serialise(self):
        data = {
            'layers': 1,
            'nodes': [1],
            'dense_layers': 1,
            'dense_nodes': [1],
            'dropouts': [1],
            'kernel_width': [1],
            'kernel_height': [1],
        }
        self.assertEqual(
            network_representation.serialise(data)['model_representation'], '001001000000000000000000000000000000000000000000000000000000010000000000000000000000000000000000000000000000000000000100000000000001000000000000010000000000000000000000000000000000000000000000001'
        )

    def test_deserialise(self):
        byte_str = '001001000000000000000000000000000000000000000000000000000000010000000000000000000000000000000000000000000000000000000100000000000001000000000000010000000000000000000000000000000000000000000000001'

        print(network_representation.deserialise(byte_str))

        
        self.assertEqual(
            1,1 
        )



if __name__ == '__main__':
    unittest.main()
